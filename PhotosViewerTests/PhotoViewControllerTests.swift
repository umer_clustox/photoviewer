//
//  PhotoViewControllerTests.swift
//  PhotosViewerTests
//
//  Created by MacBook Pro on 29/11/2018.
//  Copyright © 2018 Apple PC. All rights reserved.
//

import XCTest
@testable import PhotosViewer
@testable import Pods_PhotosViewer

class PhotoViewControllerTests: XCTestCase {

    var storyboard: UIStoryboard!
    var photosVC: PhotoViewController!
    
    override func setUp() {
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        photosVC = storyboard.instantiateViewController(withIdentifier: "PhotoViewController") as? PhotoViewController
        let _ = photosVC.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testViewDidLoad() {
        XCTAssertNotNil(photosVC.tblPhotos.dataSource)
        
        XCTAssertNotNil(photosVC.viewModel)
        XCTAssertNotNil(photosVC.viewModel.updateLoadingStatus)
        XCTAssertNotNil(photosVC.viewModel.updatePhotosList)
        XCTAssertNotNil(photosVC.viewModel.showAlert)
    }
    
    func test() {
        
    }
}
