//
//  PhotoTests.swift
//  PhotosViewerTests
//
//  Created by MacBook Pro on 30/11/2018.
//  Copyright © 2018 Apple PC. All rights reserved.
//

import XCTest
@testable import PhotosViewer

class PhotoTests: XCTestCase {

    var photo: Photo!
    
    let lat = 45.239940
    let lng = 14.250220
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        photo = Photo.init(id: 8797,
                           url: "https://splashbase.s3.amazonaws.com/newoldstock/regular/tumblr_ph8vg9TXSq1sfie3io1_1280.jpg",
                           largeUrl: "https://splashbase.s3.amazonaws.com/newoldstock/large/tumblr_ph8vg9TXSq1sfie3io1_1280.jpg",
                           sourceId: nil)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        photo = nil
    }
    
    func testPositionFallInVisibleRect() {        
        XCTAssertEqual(photo.id, 8797)
        XCTAssertEqual(photo.url, "https://splashbase.s3.amazonaws.com/newoldstock/regular/tumblr_ph8vg9TXSq1sfie3io1_1280.jpg")
        XCTAssertEqual(photo.largeUrl, "https://splashbase.s3.amazonaws.com/newoldstock/large/tumblr_ph8vg9TXSq1sfie3io1_1280.jpg")
        XCTAssertNil(photo.sourceId)
        
        photo.sourceId = 100
        XCTAssertNotNil(photo.sourceId)
        XCTAssertEqual(photo.sourceId, 100)
    }
}
