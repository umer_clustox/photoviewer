//
//  PhotoCell.swift
//  PhotosViewer
//
//  Created by Apple PC on 5/11/18.
//  Copyright © 2018 Apple PC. All rights reserved.
//

import UIKit
import Kingfisher

class PhotoCell: UITableViewCell {

    @IBOutlet weak var ivPhoto: UIImageView!
    
    var photo: Photo?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView() {
        self.ivPhoto.roundedCorners()
    }
    
    func configureData() {
        if let data = self.photo,
            let url = URL(string: data.url) {
            self.ivPhoto.kf.indicatorType = .activity
            self.ivPhoto.kf.setImage(with: url, placeholder: nil)
        }
    }

}
