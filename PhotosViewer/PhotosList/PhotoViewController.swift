//
//  PhotoViewController.swift
//  PhotosViewer
//
//  Created by Apple PC on 5/11/18.
//  Copyright © 2018 Apple PC. All rights reserved.
//

import UIKit
import Reachability

class PhotoViewController: UIViewController {

    @IBOutlet weak var tblPhotos: UITableView!
    
    let viewModel = PhotoViewModel(service: PhotoService())
    var isNetworkAvailable: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        configureData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ReachabilityManager.shared.addListener(listener: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    private func configureView() {
        tblPhotos.dataSource = self
        
        viewModel.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                let isLoading = self?.viewModel.isLoading ?? false
                if isLoading {
                    self?.startAnimating()
                } else {
                    self?.stopAnimating()
                }
            }
        }
        
        viewModel.updatePhotosList = { [weak self] () in
            DispatchQueue.main.async {
                let reloadPhotos = self?.viewModel.reloadPhotosList ?? false
                if reloadPhotos {
                    self?.tblPhotos.reloadData()
                }
            }
        }
        
        viewModel.showAlert = { [weak self] (error) in
            DispatchQueue.main.async {
                self?.showErrorAlert(error: error)
            }
        }
    }
    
    private func configureData() {
        if self.isNetworkAvailable {
            do {
                try viewModel.fetchPhotos()
            } catch let error as LocalizedError {
                self.stopAnimating()
                self.showErrorAlert(error: error.errorDescription ?? error.localizedDescription)
            } catch {
                self.stopAnimating()
                self.showErrorAlert(error: Constants.ERROR_PHOTOS_UNAVAILABLE)
            }
        } else {
            self.showErrorAlert(error: Constants.ERROR_NETWORK_UNAVAILABLE)
        }
    }
}

// MARK: - UITableViewDataSource Implementation
extension PhotoViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.viewModel.photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.PHOTO_CELL_IDENTIFIER, for: indexPath) as? PhotoCell else {
            fatalError("No cell found with identifier\(Constants.PHOTO_CELL_IDENTIFIER)")
        }
        
        let photo = self.viewModel.photos[indexPath.row]
        cell.photo = photo
        cell.configureView()
        cell.configureData()
        return cell
    }
}

extension PhotoViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.Connection) {
        
        if ReachabilityManager.shared.isNetworkAvailable {
            if !self.isNetworkAvailable {
                showSuccessAlert(message: Constants.ERROR_NETWORK_AVAILABLE)
            }
            
            self.isNetworkAvailable = true
            if self.viewModel.photos.count == 0 {
                self.configureData()
            }
        } else {
            self.isNetworkAvailable = false
            self.showErrorAlert(error: Constants.ERROR_NETWORK_UNAVAILABLE)
        }
    }
}
