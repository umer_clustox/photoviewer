//
//  PhotoController.swift
//  PhotosViewer
//
//  Created by Apple PC on 5/11/18.
//  Copyright © 2018 Apple PC. All rights reserved.
//

import UIKit

class PhotoViewModel: NSObject {

    var service: PhotoService
    
    var photos = [Photo]()
    
    var updateLoadingStatus: () -> () = {}
    var updatePhotosList: () -> () = {}
    var showAlert: ((String) -> ())?
    
    var isLoading = false {
        didSet {
            self.updateLoadingStatus()
        }
    }
    
    var reloadPhotosList = false {
        didSet {            
            self.updatePhotosList()
        }
    }
    
    init(service: PhotoService) {
        self.service = service
    }
    
    func fetchPhotos() throws {
        self.isLoading = true
        service.getPhotos() { [weak self]
            successful, error, photos in
            
            self?.isLoading = false
            
            if let nonNilerror = error,
                let showAlertFunc = self?.showAlert {
                    showAlertFunc(nonNilerror.localizedDescription)
            }
            else {
                self?.photos.removeAll()
                if let newPhotos = photos {
                    self?.photos.append(contentsOf: newPhotos)
                }
                self?.reloadPhotosList = true
            }
        }
    }
    
}
