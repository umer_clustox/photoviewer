//
//  AuthenticationService.swift
//  iOSBasicArchitechture
//
//  Created by Saira Samdani on 1/24/18.
//  Copyright © 2018 Saira Samdani. All rights reserved.
//
import UIKit
import CoreData
import Foundation

typealias ResultHandlerPhotos = (_ successful: Bool, _ error: Error?, _ photos: [Photo]?) -> ()

public struct PhotoService {
    
    let networkHandler: NetworkHandler
    
    public init() {
        self.networkHandler = NetworkHandler()
    }
    
    func getPhotos(completion: ResultHandlerPhotos? = nil) {
        
        guard let url = URL(string: Constants.URL_FETCH_IMAGES) else {
            fatalError("Malformed URL")
        }
        
        self.networkHandler.requestForGET(url: url) {
            response, error in
            
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .secondsSince1970
            
            if error != nil {
                completion?(false, error, nil)
                return
            }
            do {
                guard let responseData = response else {
                    completion!(false, error, nil)
                    return
                }
                
                let repo = try decoder.decode([String: [Photo]].self, from: responseData.data!)
                
                guard let photos = repo["images"] else {
                    completion?(false, error, nil)
                    return
                }
                
                // CORE Data Insertion
                completion?(true, nil, photos)
            } catch {
                completion?(false, error, nil)
            }
        }
    }
    
}
