//
//  Photo.swift
//  PhotosViewer
//
//  Created by Apple PC on 5/11/18.
//  Copyright © 2018 Apple PC. All rights reserved.
//
import Foundation

public struct Photo: Decodable {
    
    public var id: Int
    public var url: String
    public var largeUrl: String
    public var sourceId: Int?
    
    enum CodingKeys:String,CodingKey
    {
        case id = "id"
        case url = "url"
        case largeUrl = "large_url"
        case sourceId = "source_id"
        
    }
}
