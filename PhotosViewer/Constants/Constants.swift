//
//  Constants.swift
//  PhotosViewer
//
//  Created by Apple PC on 5/11/18.
//  Copyright © 2018 Apple PC. All rights reserved.
//

import Foundation

public struct Constants {
    
    // MARK: - URLS
    public static let BASE_URL = "http://www.splashbase.co/api/v1/images/"
    public static let URL_FETCH_IMAGES = BASE_URL + "latest"
    
    
    // MARK: - TableViewCell Identifiers
    public static let PHOTO_CELL_IDENTIFIER = "photo_cell"
    
    
    // MARK: - Errors
    public static let ERROR_NETWORK_UNAVAILABLE = "The Internet connection appears to be offline."
    public static let ERROR_PHOTOS_UNAVAILABLE = "Unable to fetch photos. Please try later."
    public static let ERROR_NETWORK_AVAILABLE = "Connected"
}
