//
//  UIViewController+Extension.swift
//  iOSBasicArchitechture
//
//  Created by Saira Samdani on 1/24/18.
//  Copyright © 2018 Saira Samdani. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import NotificationBannerSwift

extension UIViewController: NVActivityIndicatorViewable {
    
    func showAnimation() {
        DispatchQueue.main.async {
            let size = CGSize(width: 50, height:50)
            let color = UIColor.blue
            self.startAnimating(size, message: "", type: NVActivityIndicatorType.ballScaleMultiple, color: color)
        }
    }
    
    func hideAnimation() {
        DispatchQueue.main.async {
            self.stopAnimating()
        }
    }
   
    func showErrorAlert(error: String) {
        DispatchQueue.main.async {
            let banner = NotificationBanner(title: error, subtitle: "", style: .danger)
            banner.autoDismiss = true
            banner.bannerHeight = 80
            banner.show()
        }
    }
    
    func showSuccessAlert(message: String) {
        DispatchQueue.main.async {
            let banner = NotificationBanner(title: message, subtitle: "", style: .success)
            banner.autoDismiss = true
            banner.bannerHeight = 80
            banner.titleLabel?.textAlignment = .center
            banner.show()
        }
    }
    
}

