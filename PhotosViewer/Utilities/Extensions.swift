//
//  Extensions.swift
//  PhotosViewer
//
//  Created by Apple PC on 5/11/18.
//  Copyright © 2018 Apple PC. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func roundedCorners() {
        self.layer.cornerRadius = 8
        self.clipsToBounds = true
    }
}

